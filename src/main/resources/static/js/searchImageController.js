/**
 * @author Filipe Gomes
 * @since 03/04/2017
 */
(function () {
	'use strict';
	
	angular.module('app').controller('searchImageController', ['$scope', 'service', '$timeout', function ($scope, service, $timeout) {
		
		init();
		
		/**
		 * Functions
		 */
		$scope.search = search;
		$scope.cleanFilter = cleanFilter;
		$scope.openImage = openImage;
		$scope.setImagePreview = setImagePreview;
		
		function init () {
			$scope.filter = {};
			$scope.images = [];
		}
		
		function setImagePreview (image) {
			$scope.imagePreview = image;
		}
		
		function openImages (id) {
			service.getImageById(id).then(
					function (result) {
						$scope.images = result.data.attachedPictures;	
						
						if ($scope.images.length >= 1) {
							$scope.imagePreview = $scope.attachedPictures[0].attachedPicture;
						}
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		function search () {			
			$scope.images = [];
			if ($scope.filter.id) {
				service.getImageById($scope.filter.id).then(
						function (result) {
							if (result.data.id) {
								$scope.images.push(result.data);
							}														
							
						},
						function (fail) {
							console.error(fail);
							
						});
			} else {
				service.getAllImages().then(
						function (result) {
							$scope.images = result.data;
						},
						function (fail) {
							console.error(fail);
						});
			}
		}	
		
		function cleanFilter () {
			$scope.filter = {};
		}
				
	}]);
	
})();