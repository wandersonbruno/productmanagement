/**
 * @author Filipe Gomes
 * @since 29/03/2017
 */
(function () {
	'use strict';
	
	angular.module('app', ['ngRoute', 'ngResource', 'bootstrap.fileField', 'angular-loading-bar', 'ngAnimate'])
		.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
		    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
		    cfpLoadingBarProvider.spinnerTemplate = '<strong>Loading...</strong>';
		  }]);
	
})();
