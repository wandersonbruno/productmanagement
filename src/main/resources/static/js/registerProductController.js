/**
 * @author Filipe Gomes
 * @since 29/03/2017
 */
(function () {
	'use strict';
		
	angular.module('app').controller('registerProductController', ['$scope', 'service', 'editId', '$location', 'editedFilterSearch', function ($scope, service, editId, $location, editedFilterSearch) {
		
		init();
		
		/**
		 * Functions
		 */
		$scope.submit = submit;
		$scope.addImage = addImage;
		$scope.cancelImage = cancelImage;
		$scope.removeImage = removeImage;
		$scope.cleanForm = cleanForm;
		$scope.loadProductsPanel = loadProductsPanel;
		$scope.backToSearch = backToSearch;
		
		function init () {
			$scope.dto = {};
			$scope.dto.images = [];
			$scope.productsToSelect = [];
			$scope.dto.products = [];
			$scope.previewImage = undefined;
			$scope.uploadFile = undefined;
			$scope.showProductsPanel = false;
			$scope.isEdition = false;
			
			if (editId) {
				$scope.isEdition = true;
				service.getProductById(editId).then(
						function (result) {
							$scope.dto = result.data;
							$scope.dto.images = [];
							
							angular.forEach(result.data.attachedPictures, function (value) {
								var file = new File([value.attachedPicture], {type: 'image/JPEG', lastModified: Date.now()});
								$scope.dto.images.push({id: value.id, arquivo: file, preview: 'data:image/JPEG;base64,' + value.attachedPicture});
							});
							
							if (result.data.products.length >= 1) {
								loadProductsPanel();
							}
						},
						function (fail) {
							console.error(fail);
						});
			}
		}
		
		function submit () {
			$scope.dto.products = [];
			var fd = new FormData();
			if (editId) {
				fd.append('id', editId);
			}
			fd.append('name', $scope.dto.name);
			fd.append('description', $scope.dto.description)
			var i = 1;
			angular.forEach($scope.dto.images, function (value) {
				var id = (value.id ? '_'+value.id : '');
//				var id = '_'+i++;
				fd.append('file' + id, value.arquivo);
			});
			
			angular.forEach($scope.productsToSelect, function (value) {
				if (value.checked) {
					$scope.dto.products.push(value.id);
				}
			});
			
			fd.append('productsId', JSON.stringify($scope.dto.products));
			
			service.saveProduct(fd).then(
					function (result) {
						if (editId) {
							$location.path('/searchProduct').search({editedFilterSearch: editedFilterSearch});
						} else {
							cleanForm();
						}						
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		function backToSearch () {
			$location.path('/searchProduct').search({editedFilterSearch: editedFilterSearch});
		}
		
		function addImage () {
			$scope.dto.images.push({arquivo: $scope.uploadFile, preview: $scope.previewImage});
			$scope.previewImage = undefined;
			$scope.uploadFile = undefined;
		}
		
		function cancelImage () {
			$scope.previewImage = undefined;
			$scope.uploadFile = undefined;
		}
		
		function removeImage (index) {
			$scope.dto.images.splice(index, 1);
		}
		
		function loadProductsPanel () {
			service.getAllProducts().then(
					function (result) {
						$scope.productsToSelect = result.data;
						
						if (editId) {
							var productsOnlyNumber = [];
							
							angular.forEach($scope.dto.products, function (value) {
								productsOnlyNumber.push(angular.copy(value.id));
							});
							
							$scope.dto.products = productsOnlyNumber;
							
							angular.forEach($scope.productsToSelect, function (value) {
								if ($scope.dto.products.indexOf(value.id) != -1) {
									value.checked = true;
								} 
							});
						}
						
						$scope.showProductsPanel = true;
					},
					function (fail) {
						console.error(fail);
					});
		}
		
		
		
		function cleanForm () {
			init();
		}
	}]);
		
})();