package avenuecode.project.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author Filipe Gomes
 * @since 29/03/2017
 *
 */
@Entity
public class Picture implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private byte[] attachedPicture;
//	private Long productId;
	private Product product;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Lob
	public byte[] getAttachedPicture() {
		return attachedPicture;
	}

	public void setAttachedPicture(byte[] attachedPicture) {
		this.attachedPicture = attachedPicture;
	}

	@NotNull
	@Valid
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE }, optional = false)
	@JoinColumn(name = "productid", nullable = false)
	@JsonBackReference
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

//	@Column(name = "product_id")
//	public Long getProductId() {
//		return productId;
//	}
//
//	public void setProductId(Long productId) {
//		this.productId = productId;
//	}
}
