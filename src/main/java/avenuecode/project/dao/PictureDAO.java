package avenuecode.project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import avenuecode.project.entity.Picture;

/**
 * 
 * @author Filipe Gomes
 * @since 30/03/2017
 *
 */
@Repository
public interface PictureDAO extends CrudRepository<Picture, Long>{

}
