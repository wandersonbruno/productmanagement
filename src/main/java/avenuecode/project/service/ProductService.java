package avenuecode.project.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import avenuecode.project.dao.ProductDAO;
import avenuecode.project.entity.Picture;
import avenuecode.project.entity.Product;

@Service
public class ProductService {

	@Autowired
	private ProductDAO dao;

	public List<Product> getAllProduct() {
		return (List<Product>) dao.findAll();
	}

	public Product getProductById(final Long id) {
		return dao.findOne(id);
	}

	@Transactional
	public Product saveProduct(final Product product) {
		return dao.save(product);
	}

	@Transactional
	public Boolean deleteProduct(final Product product) {
		try {
			dao.delete(product);
			return Boolean.TRUE;
		} catch (Exception e) {
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	@Transactional
	public Boolean deleteAllProduct() {
		try {
			dao.deleteAll();
			return Boolean.TRUE;
		} catch (Exception e) {
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	public List<Picture> bytePictures(final List<FormDataBodyPart> bodyParts) throws IOException {
		Picture img;
		List<Picture> images = new ArrayList<Picture>();

		if (bodyParts != null) {
//			for (int i = 0; i < bodyParts.size(); i++) {
//				BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
//				final byte[] bytes = IOUtils.toByteArray(bodyPartEntity.getInputStream());
//				img = new Picture();
//				img.setAttachedPicture(bytes);
//				images.add(img);
//			}
			
			for (FormDataBodyPart file: bodyParts) {
		        FormDataContentDisposition fdcd = file.getFormDataContentDisposition();
		        String splitFileName[] = fdcd.getFileName().split("-");
		        String id = splitFileName[0];
		        InputStream is = file.getValueAs(InputStream.class);
		        final byte[] bytes = IOUtils.toByteArray(is);
		        img = new Picture();
		        img.setId(id != null ? Long.parseLong(id) : null);
				img.setAttachedPicture(bytes);
				images.add(img);
		    }
		}

		return images;
	}

	public List<Product> arrayProducts(final String productsId) {
		Product productChild;
		List<Product> products = new ArrayList<Product>();
		final String splitProductsId[] = productsId.split(",");

		for (int i = 0; i < splitProductsId.length; i++) {
			if (!splitProductsId[i].equals("")) {
				productChild = getProductById(Long.parseLong(splitProductsId[i]));
				if (productChild != null) {
					products.add(productChild);
				}
			}
		}

		return products;
	}

}
