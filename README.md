## Synopsis

RESTful Web Service system with JAX-RS for product management. This system serves to consult, read, delete, update and register products and sub products with images. The frontend was made with AngularJS and Bootstrap. This project uses the following technologies:

- JDK 1.8
- Spring Boot 1.5.2
- H2Database 1.4
- Jersey 2.25
- AngularJS 1.6.2
- BootstrapJS 3.3.7
- JQuery 3.2.0
- Hibernate 5.0.1
- JPA 2.1


## Code Example

We have practical, concise ways of making calls to the backend. Calls can be made as follows using any application that is able to make the requests:

GET all products: http://localhost:8080/product (Specifying the GET request)

GET product by id: http://localhost:8080/product/<id> (Specifying the GET request)

GET all images: http://localhost:8080/product/image (Specifying the GET request)

GET image by id: http://localhost:8080/product/image/<id> (Specifying the GET request)

POST a product to INSERT or UPDATE specifing que Json data: http://localhost:8080/product (Specifying the POST request)
Json data to POST example: 
{id: 1, name: "productTest", description: "productDesc", attachedPictures: [{id: null,…}], products: []}

DELETE a product by id: http://localhost:8080/product/<id> (Specifying the DELETE request)

DELETE all products: http://localhost:8080/product (Specifying the DELETE request)

## Motivation

The biggest motivation was the challenge of working with Spring Boot, which is a technology I had little access to. I also have a great desire to demonstrate a little of my competence to the evaluators.

## Installation

To start the project you must configure the arguments by running:

mvn spring-boot: run

When the application has started just access the url directly:

http://localhost:8080

## API Reference

The Product Management API allows you to programmatically record, query, delete, and update a product's data, as well as record images and by-products.

This API uses JSON in its first and only version and a way of working:

- Requests and responses between clients are encoded in JSON format.

An API is made up of a number of methods that are nominally separated as individual APIs. Each of the methods run on a specific task. For example, the getAllProduct method belongs to the Query API and is used to create data that is returned by the Response. Historically as APIs they are like "classes".

## Contributors

Data Model and User History: Avenue Code
Code: Filipe Souza Sabino Gomes